MD5sumJ
=======

This program is helpfully to verify the checksum with MD5, SHA1, SHA256 and so on, in console or, if prefer, in windows mode.

__How to use:__
java -jar comand [opts]

_commands_:

    -h: help mode
    -g: graphic mode
    -c: console mode
    -ch: console help mode

_options_ (for command -c;  option -t and -fi needed):

    -t: tipology of checksum (MD5, SHA-1, SHA-256, SHA-384, SHA-512)
    -fi: file interested
    -fc: file containing checksum (can't be -ck option) 
    -ck: checksum (can't be -fc option)
